// navid mirmoghtadaei 9421170036
#include <iostream>
using namespace std;
class tree{
	public:
	string data;
	tree *left;
	tree *right;
	tree(){
		left=0;
		right=0;
	}
};
void sathi(tree *head){
	tree *main;
	main = head;
	static int here=1;
	static int where=0;
	tree *temp[100];
	temp[0] = main;
	for(int i=1;i<100;i++){
		if(temp[where]){
			temp[here] = temp[where]->left;
			here++;
			temp[here] = temp[where]->right;
			here++;
			where++;
		}
	}
	int i = 0;
	while(temp[i]){
		cout<<temp[i]->data;
		i++;
	}
}
void postorder(tree *head){
	if(head){
		postorder(head->left);
		postorder(head->right);
		cout << head->data;	
	}
}
void inorder(tree *head){
	if(head){
		inorder(head -> left);
		cout << head->data;
		inorder(head->right);
	}	
}
void preorder(tree *head){
	if(head){
		cout << head->data;
		preorder(head->left);
		preorder(head->right);
	}
}
int main(){
	tree *head;
	head = new tree;
	head -> data = "1";
	head -> left = new tree;
	head -> left -> data = "2";
	head -> left -> left = new tree;
	head -> left -> right = new tree;
	head -> left -> left -> data = "3";
	head -> left -> right -> data = "4";
	head -> left -> right -> right = new tree;
	head -> left -> right -> right -> data = "5";
	head -> right = new tree;
	head -> right -> data = "6";
	head -> right -> left = new tree;
	head -> right -> right = new tree;
	head -> right -> left -> data = "7";
	head -> right -> right -> data = "8";
	head -> right -> right -> right = new tree;
	head -> right -> right -> right -> data = "9";
	cout << "nemayeshe sathi : " ;
	sathi(head);
	cout << endl << "nemayeshe pasvandi : " ;
	postorder(head);
	cout << endl << "nemayeshe mianvandi : " ;
	inorder(head);
	cout << endl << "nemayeshe pishvandi : " ;
	preorder(head);
	return 0;
}
